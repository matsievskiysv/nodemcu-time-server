function ram_page_get() {
	fetch("/ram.lua", {
		method: "GET",
		cache: "no-cache",
		headers: {
			"Accept": "text/plain",
		},
	}).then(res => {
		if (res.ok)
			res.text().then(res => document.getElementById("text").value = res.trim());
	});
}

function ram_page_set() {
	fetch("/ram.lua", {
		method: "POST",
		headers: {
			"Content-Type": "text/plain",
		},
		body: document.getElementById("text").value.trim(),
	});
}

function apply_read_numstyle() {
	const table = document.getElementById("table");
	const numbase = parseInt(document.getElementById("numstyle").value);
	Array.from(table.children).forEach(row => {
		const cell = row.children[1];
		const val = cell.getAttribute("raw-value");
		const valInt = parseInt(val === null ? 0 : val);
		cell.textContent = valInt.toString(numbase);
	});
}

function apply_write_numstyle() {
	const table = document.getElementById("table");
	const numbase = parseInt(document.getElementById("numstyle").value);
	Array.from(table.children).forEach(row => {
		const cell = row.children[2];
		const input = cell.children[0];
		const val = cell.getAttribute("raw-value");
		const valInt = parseInt(val === null ? 0 : val);
		input.value = valInt.toString(numbase);
	});
}

function apply_numstyle() {
	apply_read_numstyle();
	apply_write_numstyle();
}

function parse_time(arr) {
	const timeString = document.getElementById("time");
	const time = timeString.valueAsDate;
	time.setUTCFullYear(((arr[6] >> 4) & 0xf) * 10 + (arr[6] & 0xf) + 2000);
	time.setUTCMonth(((arr[5] >> 4) & 1) * 10 + (arr[5] & 0xf));
	time.setUTCDate(((arr[4] >> 4) & 0x3) * 10 + (arr[4] & 0xf));
	if ((arr[2] >> 7) & 1) {
		const pm = ((arr[2] >> 6) & 1);
		time.setUTCHours(((arr[2] >> 4) & 1) * 10 + (arr[2] & 0xf) + pm * 12);
	} else {
		time.setUTCHours(((arr[2] >> 4) & 0x3) * 10 + (arr[2] & 0xf));
	}
	time.setUTCMinutes(((arr[1] >> 4) & 0x7) * 10 + (arr[1] & 0xf));
	time.setUTCSeconds(((arr[0] >> 4) & 0x7) * 10 + (arr[0] & 0xf));
	timeString.value = time.toISOString().split(".")[0];
}

function time_page_get() {
	fetch("/time.lua", {
		method: "GET",
		cache: "no-cache",
		headers: {
			"Accept": "application/json",
		},
	}).then(res => {
		if (res.ok)
			res.json().then(arr => {
				const table = document.getElementById("table");
				arr.forEach((val, index) => {
					const row = table.children[index];
					const cell = row.children[1];
					cell.setAttribute("raw-value", val);
				});
				apply_read_numstyle();
				parse_time(arr);
			});
	});
}

function time_page_set() {
	const data = Array.from(document.getElementById("table").children)
		.map(row => {
			const rawValue = row.children[2].getAttribute("raw-value");
			const value = rawValue === null ? 0 : parseInt(rawValue);
			return value;
		});
	fetch("/time.lua", {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify(data),
	});
}

function validate_cell(cell) {
	let validator;
	const value = cell.children[0].value;
	const numstyle = document.getElementById("numstyle").value;
	const numbase = parseInt(numstyle);
	switch (numstyle) {
		case "2":
			validator = /^[0-1]{1,8}$/;
			break;
		case "10":
			validator = /^[0-9]{1,3}$/;
			break;
		case "16":
			validator = /^[0-9a-fA-F]{1,2}$/;
			break;
	}
	cell.classList.add("invalid");
	const valueInt = parseInt(value, numbase);
	if (validator.test(value) && valueInt >= 0 && valueInt <= 0xff) {
		cell.setAttribute("raw-value", valueInt);
		cell.classList.remove("invalid");
	}
}

function ram_page_setup() {
	document.getElementById("get").addEventListener("click", ram_page_get);
	document.getElementById("set").addEventListener("click", ram_page_set);
}

function time_page_setup() {
	document.getElementById("get").addEventListener("click", time_page_get);
	document.getElementById("set").addEventListener("click", time_page_set);
	document.getElementById("numstyle").addEventListener("change", apply_numstyle);
	Array.from(document.getElementById("table").children).forEach(row => {
		const cell = row.children[2];
		cell.children[0].addEventListener("input", () => validate_cell(cell));
	});
}

export { ram_page_setup, time_page_setup };
