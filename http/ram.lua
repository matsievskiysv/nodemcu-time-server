local function sendResponse(connection, httpCode, data)
   connection:send("HTTP/1.0 "..httpCode.." OK\r\nContent-Type: text/plain\r\nCache-Control: private, no-store\r\n\r\n")
   connection:send(data)
end

return function (connection, req, args)

   if req.method == "GET" then
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.TRANSMITTER)
      i2c.write(_G.TIME.id, 8)
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.RECEIVER)
      local data = i2c.read(_G.TIME.id, 56)
      i2c.stop(_G.TIME.id)
      sendResponse(connection, 200, data)
      return
   elseif req.method == "POST" then
      local data = req.getRequestData()
      local stripped_data = data[1]:gsub("[\r\n]", "")
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.TRANSMITTER)
      i2c.write(_G.TIME.id, 8)
      for i=1,56 do
         i2c.write(_G.TIME.id, 32)
      end
      i2c.stop(_G.TIME.id)
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.TRANSMITTER)
      i2c.write(_G.TIME.id, 8)
      i2c.write(_G.TIME.id, stripped_data:sub(1, 56))
      i2c.stop(_G.TIME.id)
      sendResponse(connection, 200, "ok")
      return
   end

   sendResponse(connection, 400, "error")
end
