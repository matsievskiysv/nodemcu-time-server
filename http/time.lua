local function sendResponse(connection, httpCode, data)
   connection:send("HTTP/1.0 "..httpCode.." OK\r\nContent-Type: text/plain\r\nCache-Control: private, no-store\r\n\r\n")
   connection:send(data)
end

local function tojsonarray(data)
   s = ""
   for i=1,#data do s = s..","..tostring(data:byte(i)) end
   return "[" .. s:sub(2) .. "]"
end

return function (connection, req, args)
   if req.method == "GET" then
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.TRANSMITTER)
      i2c.write(_G.TIME.id, 0)
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.RECEIVER)
      local data = i2c.read(_G.TIME.id, 8)
      i2c.stop(_G.TIME.id)
      sendResponse(connection, 200, tojsonarray(data))
      return
   elseif req.method == "POST" then
      local data = req.getRequestData()
      i2c.start(_G.TIME.id)
      i2c.address(_G.TIME.id, _G.TIME.addr, i2c.TRANSMITTER)
      i2c.write(_G.TIME.id, 0)
      for _, byte in pairs(data) do
         i2c.write(_G.TIME.id, byte)
      end
      i2c.stop(_G.TIME.id)
      sendResponse(connection, 200, "ok")
      return
   end
   sendResponse(connection, 400, "error")
end
