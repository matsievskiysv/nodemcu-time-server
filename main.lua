_G.TIME = {
   id = 0,
   addr = 0x68,
   sda = 5,
   scl = 6,
   speed = i2c.SLOW,
}

i2c.setup(_G.TIME.id, _G.TIME.sda, _G.TIME.scl, _G.TIME.speed)
i2c.start(_G.TIME.id)
i2c.address(_G.TIME.id, _G.TIME.addr, i2c.TRANSMITTER)
i2c.write(_G.TIME.id, 8)
for i=1,56 do
   i2c.write(_G.TIME.id, 32)
end
i2c.stop(_G.TIME.id)
